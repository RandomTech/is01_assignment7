#include<stdio.h>
#define ROWS 3
#define COLS 3

void add(int matA[ROWS][COLS], int matB[ROWS][COLS]);
void multiply(int matA[ROWS][COLS], int matB[ROWS][COLS]);
void print(int mulArr[ROWS][COLS]);

int main(){
	int matA[ROWS][COLS]={1,2,4,5,6,7,5,12,6};
	int matB[ROWS][COLS]={5,6,7,10,11,5,40,20,5};
	int answer[ROWS][COLS];
	
	printf("Addition of the two matrices is\n");
	add(matA, matB);
	printf("Multiplication of the two matrices is\n");
	multiply(matA, matB);
	
	return 0;
}

void add(int matA[ROWS][COLS], int matB[ROWS][COLS]){
	int ans[ROWS][COLS];
	for(int i=0; i<ROWS; i++){
		for(int j=0; j<COLS; j++){
			ans[i][j] = matA[i][j] + matB[i][j];
		}
	}
	print(ans);
}

void multiply(int matA[ROWS][COLS], int matB[ROWS][COLS]){
	int ans[ROWS][COLS];
	for(int i=0; i<ROWS; i++){
		for(int j=0; j<COLS; j++){
			ans[i][j] = matA[i][j] * matB[i][j];
		}
	}
	print(ans);
}

void print(int mulArr[ROWS][COLS]){
	for(int i=0; i<ROWS; i++){
		for(int j=0; j<COLS; j++){
			printf("%d \t", mulArr[i][j]);
		}
	printf("\n");
	}
}