#include<stdio.h>
#define SIZE 50
int main(){
	char c, count;
	char sen[SIZE];
	printf("Enter a string: ");
	fgets(sen,sizeof(sen),stdin);
	
	printf("Enter the search character: ");
	scanf("%c", &c);
	
	for(int i=0; i<sizeof(sen); i++){ 
		if(sen[i] == c)
			count++;
	}
	printf("Frequency of %c in the above sentence= %d\n", c, count);
	return 0;
} 